# GameHub

Projet réalisé par Adrien Ossywa, Nathan Steger et Raphaël Schwehr.

## Utilisation
* L'application a été build dans l'apk disponible à la racine de ce repo.
* L'API est hébergée sur un serveur distant (217.182.67.154:3000)

Dans le cas où l'api semble inaccessible pendant vos tests vous pouvez nous contacter rapidement par message privé sur Discord (nous avons nos noms visibles sur le serveur de programmation mobile)
