package com.gamehubinc.gamehub;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.RequestFuture;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class API {

    public API(){};

    private static final String API_URL = "http://217.182.67.154:3000/";


    /**
     * Calls the login route of the API and instantiate User singleton if success
     * @param context the Activity context
     * @param pseudo player's name
     * @param pwd player's password
     */
    public static void logIn (Context context, String pseudo, String pwd){

        String url =API_URL+"users/verify";

        JSONObject user = new JSONObject();
        try{
            user.put("username",pseudo);
            user.put("password",pwd);
        }catch (JSONException e) {
            Toast.makeText(context,
                    "Erreur de connexion ❌",
                    Toast.LENGTH_SHORT).show();
        }

        AlertDialog alert = new AlertDialog.Builder(context).setMessage("Connexion ... \uD83D\uDCA4").setCancelable(false).create();
        alert.show();

        RequestQueue queue = Volley.newRequestQueue(context);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (Request.Method.POST, url, user, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {

                        try{
                            Intent intent = new Intent(context, HomePageActivity.class);

                            //User's instantiation
                            User user = User.getInstance();
                            user.setId(response.getInt("id"));
                            user.setName(response.getString("username"));
                            user.setOnline(true);

                            context.startActivity(intent);

                        }catch(JSONException e){
                            e.printStackTrace();
                        }

                        alert.dismiss();

                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(context,
                                "Erreur de connexion ❌",
                                Toast.LENGTH_SHORT).show();

                        alert.dismiss();
                    }
                });

        queue.add(jsonObjectRequest);
    }

    /**
     * Calls the account creation API route and goes to LoginOnlineFragment in case of success
     * @param context activity context
     * @param pseudo player's name
     * @param pwd player's password
     * @param frag CreateAccFragment
     */
    public static void createAcc (Context context, String pseudo, String pwd, Fragment frag){

        String url =API_URL+"users";

        AlertDialog alert = new AlertDialog.Builder(context).setMessage("Création de compte ... \uD83D\uDCA4 ").setCancelable(false).create();
        alert.show();

        RequestQueue queue = Volley.newRequestQueue(context);

        StringRequest  stringRequest  = new StringRequest
                (Request.Method.POST, url, new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {

                        Toast.makeText(context,
                                "Compte créé ! Have fun \uD83D\uDE01",
                                Toast.LENGTH_SHORT).show();

                        alert.dismiss();

                        NavHostFragment.findNavController(frag)
                                .navigate(R.id.action_createAccFragment_to_loginOnlineFragment);

                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {

                        Toast.makeText(context,
                                "Erreur lors de la création de compte \uD83D\uDE30",
                                Toast.LENGTH_SHORT).show();

                        alert.dismiss();
                    }
                }){
            //adding parameters to the request
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("username", pseudo);
                params.put("password", pwd);
                return params;
            }
            };

        queue.add(stringRequest);
    }


    /**
     * Calls the getScores method with 'personnal=false'
     * @param context activity context
     * @param gameName the game's name to get score of
     * @param getQuery get query (ie : '?bound=100')
     */
    public static void getScores(Context context, String gameName, String getQuery){
        getScores(context,gameName,false,getQuery);
    }

    /**
     * Calls the GET scores route of the API
     * @param context activity context
     * @param gameName the game's name to get score of
     * @param personnal true for user's scores, false for world scores
     * @param getQuery get query (ie : '?bound=100')
     */
    public static void getScores(Context context, String gameName, boolean personnal, String getQuery){
        String uid = "";
        if(personnal){
            uid = "/"+User.getInstance().getId();
        }
        String url =API_URL+"scores/"+gameName+uid+getQuery;
        RequestQueue queue = Volley.newRequestQueue(context);
        AlertDialog alert = new AlertDialog.Builder(context).setMessage("Patientez ...").setCancelable(false).create();
        alert.show();
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (Request.Method.GET, url,null, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        ScoresActivity scoreScreen = (ScoresActivity) context;
                        scoreScreen.displayScores(response);
                        alert.dismiss();
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {

                        System.out.println(error.toString());
                        ScoresActivity scoreScreen = (ScoresActivity) context;
                        Toast.makeText(context,
                                "Erreur lors de la récupération des scores",
                                Toast.LENGTH_SHORT).show();

                        alert.dismiss();
                    }
                });
        queue.add(jsonObjectRequest);
    }

    /**
     * Calls the postScore method with 'bound=-1'
     * @param context activity context
     * @param gameName the game's name to post a score
     * @param score score's value
     */
    public static void postScore(Context context, String gameName, int score) {
        postScore(context,gameName,score,-1);
    }

    /**
     * Calls the POST scores route of the API
     * @param context activity context
     * @param gameName the game's name to post a score
     * @param score score's value
     * @param bound the bound (for mistery_number only)
     */
    public static void postScore(Context context, String gameName, int score, int bound){
        String url =API_URL+"scores/"+gameName;
        RequestQueue queue = Volley.newRequestQueue(context);
        Toast.makeText(context,
                "Publication du score...",
                Toast.LENGTH_SHORT).show();
        StringRequest stringRequest = new StringRequest
                (Request.Method.POST, url, new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                        Toast.makeText(context,
                                "Score publié",
                                Toast.LENGTH_SHORT).show();
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {

                        System.out.println(error.toString());
                        error.printStackTrace();
                        Toast.makeText(context,
                                "Erreur lors de la publication du score",
                                Toast.LENGTH_SHORT).show();
                    }
                }){
                //adding parameters to the request
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> requestParams = new HashMap<>();
                    requestParams.put("userId", ""+ User.getInstance().getId());
                    requestParams.put("score", ""+ score);
                    if(bound > 0){
                        requestParams.put("bound",""+bound);
                    }
                    return requestParams;
                }
            };
        queue.add(stringRequest);
    }

    /**
     * The PUT scores/morpion of the API
     * @param context activity context
     */
    public static void putMorpionScore(Context context){
        String url =API_URL+"scores/morpion/"+User.getInstance().getId();
        RequestQueue queue = Volley.newRequestQueue(context);
        Toast.makeText(context,
                "Incrémentation du score...",
                Toast.LENGTH_SHORT).show();
        StringRequest stringRequest = new StringRequest
                (Request.Method.PUT, url, new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                        Toast.makeText(context,
                                "Score mis à jour",
                                Toast.LENGTH_SHORT).show();
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {

                        System.out.println(error.toString());
                        error.printStackTrace();
                        Toast.makeText(context,
                                "Erreur lors de la mise à jour du score",
                                Toast.LENGTH_SHORT).show();
                    }
                });
        queue.add(stringRequest);
    }


}
