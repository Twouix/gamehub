package com.gamehubinc.gamehub;


public class User {
    private int id;
    private String name;
    private boolean online;

    private static User instance = null;

    private User(int id, String name,  boolean online){
        this.id = id;
        this.name = name;
        this.online = online;
    }

    /**
     * Return the User's instance (and creates it if it's the first time)
     * @return the User's singleton instance
     */
    public static User getInstance(){
        if(instance == null){
            instance = new User(-1,"Unnamed",false);
        }
        return instance;
    }

    /**
     * Resets the singleton instance
     */
    public static void reset(){
        instance.id = -1;
        instance.name = "Unnamed";
        instance.online = false;
    }

    public String getName(){
        return this.name;
    }

    public int getId(){
        return this.id;
    }

    public boolean isOnline(){
        return this.online;
    }

    public void setId(int id){
        this.id = id;
    }

    public void setName(String name){
        this.name = name;
    }

    public void setOnline(boolean online){
        this.online = online;
    }
}
