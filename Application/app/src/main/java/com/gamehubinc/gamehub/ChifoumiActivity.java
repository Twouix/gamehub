package com.gamehubinc.gamehub;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONObject;

import java.util.Random;

public class ChifoumiActivity extends AppCompatActivity {

    private User user;
    private Button btnContinue, btnQuit, btnScore;
    private int choiceP1, choiceP2, scoreP1, scoreP2;
    private Random random;
    private TextView txtMatchOver, txtVS, txtScoreLeft, txtScoreRight, txtScoreSudden, txtInfo;
    private ImageView imgChoiceP1, imgChoiceP2;
    private boolean isGameOver = false, isMatchOver = false;
    private char mode = 0, currentPlayer = 0;
    private final int SCORE_TO_WIN = 5;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chifoumi_main);

        random = new Random();
    }

    /**
     * Finishes the activity
     * @param view
     */
    public void leave(View view){
        finish();
    }

    /**
     * Launches a game in sudden death mode
     * @param view
     */
    public void launchSuddenDeath(View view){
        mode = 0;
        launchGame(view);

    }

    /**
     * Launches a game in P1vsCOM mode
     * @param view
     */
    public void launchP1vsCOM(View view){
        mode = 1;
        launchGame(view);
    }

    /**
     * Launches a game in P1vsP2 mode
     * @param view
     */
    public void launchP1vsP2(View view){
        mode = 2;
        launchGame(view);
    }

    /**
     * Chooses the rock (for the current player)
     * @param view
     */
    public void rock(View view){
        if(mode != 2){
            choiceP1 = 0;
            play();
        }
        else if (!isMatchOver){
            if(currentPlayer == 0){
                choiceP1=0;
            }
            else {
                choiceP2=0;
            }
            nextPlayer();
        }
    }

    /**
     * Chooses the paper (for the current player)
     * @param view
     */
    public void paper(View view){
        if(mode != 2){
            choiceP1 = 1;
            play();
        }
        else if (!isMatchOver){
            if(currentPlayer == 0){
                choiceP1=1;
            }
            else {
                choiceP2=1;
            }
            nextPlayer();
        }
    }

    /**
     * Chooses the scissors (for the current player)
     * @param view
     */
    public void scissors(View view){
        if(mode != 2){
            choiceP1 = 2;
            play();
        }
        else if (!isMatchOver){
            if(currentPlayer == 0){
                choiceP1=2;
            }
            else {
                choiceP2=2;
            }
            nextPlayer();
        }
    }

    /**
     * Resolves the chifoumi match
     */
    public void play(){

        if(!isMatchOver) {
            if(mode != 2){ //If COM plays
                choiceP2 = random.nextInt(3);
            }
            switch (choiceP1) {
                case 0:
                    imgChoiceP1.setImageResource(R.drawable.rock);
                    break;
                case 1:
                    imgChoiceP1.setImageResource(R.drawable.paper);
                    break;
                case 2:
                    imgChoiceP1.setImageResource(R.drawable.scissors);
                    break;
            }

            switch (choiceP2) {
                case 0:
                    imgChoiceP2.setImageResource(R.drawable.rock);
                    break;
                case 1:
                    imgChoiceP2.setImageResource(R.drawable.paper);
                    break;
                case 2:
                    imgChoiceP2.setImageResource(R.drawable.scissors);
                    break;
            }
            if (choiceP1 == choiceP2) {
                txtMatchOver.setText("Égalité");
            } else if (choiceP1 == 0) {//Rock
                if (choiceP2 == 1) {//Paper
                    if(mode == 0){
                        gameOver();
                    }
                    else {
                        if(mode == 1){ // 1vsCOM
                            txtMatchOver.setText("Défaite ...");
                        }
                        else { // 1vs2
                            txtMatchOver.setText("Victoire J2 !");
                        }
                        scoreP2++;
                    }
                } else { // choiceJ2 == 2 Scissors
                    if(mode == 2){ // 1vs2
                        txtMatchOver.setText("Victoire J1 !");
                    }
                    else {
                        txtMatchOver.setText("Victoire !");
                    }
                    scoreP1++;
                }
            } else if (choiceP1 == 1) {//Paper
                if (choiceP2 == 0) {//Rock
                    if(mode == 2){ // 1vs2
                        txtMatchOver.setText("Victoire J1 !");
                    }
                    else {
                        txtMatchOver.setText("Victoire !");
                    }
                    scoreP1++;
                } else { // choiceJ2 == 2 Scissors
                    if(mode == 0){
                        gameOver();
                    }
                    else {
                        if(mode == 1){ // 1vsCOM
                            txtMatchOver.setText("Défaite ...");
                        }
                        else { // 1vs2
                            txtMatchOver.setText("Victoire J2 !");
                        }
                        scoreP2++;
                    }
                }
            } else if (choiceP1 == 2) {//Scissors
                if (choiceP2 == 0) {//Rock
                    if(mode == 0){
                        gameOver();
                    }
                    else {
                        if(mode == 1){ // 1vsCOM
                            txtMatchOver.setText("Défaite ...");
                        }
                        else { // 1vs2
                            txtMatchOver.setText("Victoire J2 !");
                        }
                        scoreP2++;
                    }
                } else { // choiceJ2 == 1 Paper
                    if(mode == 2){ // 1vs2
                        txtMatchOver.setText("Victoire J1 !");
                    }
                    else {
                        txtMatchOver.setText("Victoire !");
                    }
                    scoreP1++;
                }
            }

            updateScoreTexts();
            setMatchOverItemsVisibility(View.VISIBLE);
            isMatchOver = true;
            if(mode != 0 && (scoreP1 >= SCORE_TO_WIN || scoreP2 >= SCORE_TO_WIN)){
                gameOver();
            }
        }
    }

    /**
     * Continues the game, launches next match (or game if game is over)
     * @param view
     */
    public void continueGame(View view){
        setMatchOverItemsVisibility(View.INVISIBLE);
        isMatchOver = false;
        if(isGameOver){
            launchGame(view);
        }
        else if(mode == 2){
            txtInfo.setVisibility(View.VISIBLE);
        }
    }

    /**
     * Sets up back the chifoumi main menu
     * @param view
     */
    public void quitGame(View view){
        setContentView(R.layout.activity_chifoumi_main);
    }

    /**
     * Creates a score activity and launches it (only if User.isOnline())
     * @param view
     */
    public void scores(View view){
        if(User.getInstance().isOnline()){
            Intent intent = new Intent(this, ScoresActivity.class); //Nav vers la nouvelle activit
            intent.putExtra("gameName","chifoumi");
            startActivity(intent);
        }
        else{
            Toast.makeText(this,
                    "Vous devez être connecté !",
                    Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Switches 'match over' related elements to the given visibility mode
     * @param mode visibilyti mode (View.VISIBLE, View.INVISIBLE, ...)
     */
    private void setMatchOverItemsVisibility(int mode){
        imgChoiceP1.setVisibility(mode);
        imgChoiceP2.setVisibility(mode);
        txtVS.setVisibility(mode);
        txtMatchOver.setVisibility(mode);
        btnContinue.setVisibility(mode);
        btnQuit.setVisibility(mode);
    }

    /**
     * Updates scores texts with internal scores variables
     */
    private void updateScoreTexts(){
        txtScoreLeft.setText(""+scoreP1);
        txtScoreSudden.setText(""+scoreP1);
        txtScoreRight.setText(""+scoreP2);
    }

    /**
     * Sets up the game view and every related parameters
     * @param view
     */
    private void launchGame(View view){
        setContentView(R.layout.chifoumi_game);
        isGameOver=false;
        scoreP1 = 0;
        scoreP2 = 0;
        txtScoreLeft = this.findViewById(R.id.txtScoreLeft);
        txtScoreRight = this.findViewById(R.id.txtScoreRight);
        txtScoreSudden = this.findViewById(R.id.txtScoreSudden);
        btnContinue = this.findViewById(R.id.btnNext);
        btnContinue.setText("Continuer");
        btnQuit = this.findViewById(R.id.btnQuit);
        btnScore = this.findViewById(R.id.btnScore);
        txtMatchOver = this.findViewById(R.id.txtMatchOver);
        txtInfo = this.findViewById(R.id.txtInfo);
        txtVS = this.findViewById(R.id.txtVS);
        imgChoiceP1 = this.findViewById(R.id.imgChoiceP1);
        imgChoiceP2 = this.findViewById(R.id.imgChoiceP2);
        isMatchOver = false;
        if(mode == 0) {  //sudden death
            txtScoreLeft.setVisibility(View.INVISIBLE);
            txtScoreRight.setVisibility(View.INVISIBLE);
            txtScoreSudden.setVisibility(View.VISIBLE);
        }
        else{ // 1vsCOM || 1vs2
            txtScoreLeft.setVisibility(View.VISIBLE);
            txtScoreRight.setVisibility(View.VISIBLE);
            txtScoreSudden.setVisibility(View.INVISIBLE);
        }
        btnScore.setVisibility(View.INVISIBLE);
        if(mode == 2){
            txtInfo.setText("Tour de J1");
            txtInfo.setVisibility(View.VISIBLE);
        }
        else {
            txtInfo.setVisibility(View.INVISIBLE);
        }
        setMatchOverItemsVisibility(View.INVISIBLE);
        updateScoreTexts();
    }

    /**
     * Sets up the 'game over' screen
     */
    private void gameOver(){
        isGameOver=true;
        btnContinue.setText("Recommencer");
        if(mode == 0) {
            txtMatchOver.setText("Défaite ...");
            btnScore.setVisibility(View.VISIBLE);

            if(User.getInstance().isOnline()){
                API.postScore(this,"chifoumi",scoreP1);
            }
        }
        else {
            txtInfo.setText("Partie terminée");
            txtInfo.setVisibility(View.VISIBLE);
        }
    }

    /**
     * Switches to next player (and calls play() method if currentPlayer==1 when called)
     */
    private void nextPlayer(){
        currentPlayer++;
        if(currentPlayer == 1){ //Player 2
            txtInfo.setText("Tour de J2");
        }
        if(currentPlayer > 1){ //Play + return to player 1
            txtInfo.setVisibility(View.INVISIBLE);
            txtInfo.setText("Tour de J1");
            currentPlayer = 0;
            play();
        }
    }
}