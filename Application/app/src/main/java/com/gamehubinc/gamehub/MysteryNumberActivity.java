package com.gamehubinc.gamehub;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.opengl.Visibility;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Random;

public class MysteryNumberActivity extends AppCompatActivity {

    private TextView txtValue, txtMysteryInfo, txtClue, txtTriesP1, txtTriesP2, txtMaxBound;
    private Button btn1, btn2, btn3, btn4, btn5, btn6, btn7, btn8, btn9, btn0, btnValidate, btnBackSpace, btnReplayMystery, btnChangeMax, btnQuitMystery, btnScoresMystery;
    private int mysteryNumber = -1, max = 0, triesP1 = 0, triesP2 = 0;
    private Random random;
    private boolean isGameOver = false;

    private User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mystery_number);

        Intent intent = getIntent();
        if (intent != null){
            User userParse = intent.getParcelableExtra("user");
            if (userParse != null){
                user = userParse;
            }
        }

        random = new Random();
    }

    /**
     * Finishes the activity
     * @param view
     */
    public void leave(View view){
        finish();
    }

    /**
     * Sets up the game view and every related parameters
     * @param view
     */
    public void launchSolo(View view){
        setContentView(R.layout.mystery_number_game);
        max = 0;
        isGameOver = false;
        triesP1 = 0;
        triesP2 = 0;
        txtValue = this.findViewById(R.id.txtValue);
        txtMysteryInfo = this.findViewById(R.id.txtMysteryInfo);
        txtClue =  this.findViewById(R.id.txtClue);
        txtTriesP1 = this.findViewById(R.id.txtTriesP1);
        txtTriesP2 = this.findViewById(R.id.txtTriesP2);
        txtMaxBound = this.findViewById(R.id.txtMaxBound);
        btn1 = this.findViewById(R.id.btn1);
        btn2 = this.findViewById(R.id.btn2);
        btn3 = this.findViewById(R.id.btn3);
        btn4 = this.findViewById(R.id.btn4);
        btn5 = this.findViewById(R.id.btn5);
        btn6 = this.findViewById(R.id.btn6);
        btn7 = this.findViewById(R.id.btn7);
        btn8 = this.findViewById(R.id.btn8);
        btn9 = this.findViewById(R.id.btn9);
        btn0 = this.findViewById(R.id.btn0);
        btnValidate = this.findViewById(R.id.btnValidate);
        btnBackSpace = this.findViewById(R.id.btnBackSpace);
        btnReplayMystery = this.findViewById(R.id.btnReplayMystery);
        btnChangeMax = this.findViewById(R.id.btnChangeMax);
        btnQuitMystery = this.findViewById(R.id.btnQuitMystery);
        btnScoresMystery = this.findViewById(R.id.btnScoresMystery);
        txtValue.setText("");
        txtMysteryInfo.setText("Choisissez la borne maximum :");
        txtClue.setText("");
        txtTriesP1.setText("Essais : 0");
        txtTriesP2.setText("Essais : 0");
        txtMaxBound.setText("Borne : -");
        setEndGameButtonVisibility(View.INVISIBLE);
        setKeyBoardVisibility(View.VISIBLE);

    }

    /**
     * Calls typeNumber("1")
     * @param view
     */
    public void type1(View view){
        typeNumber("1");
    }
    /**
     * Calls typeNumber("2")
     * @param view
     */
    public void type2(View view){
        typeNumber("2");
    }
    /**
     * Calls typeNumber("3")
     * @param view
     */
    public void type3(View view){
        typeNumber("3");
    }
    /**
     * Calls typeNumber("4")
     * @param view
     */
    public void type4(View view){
        typeNumber("4");
    }
    /**
     * Calls typeNumber("5")
     * @param view
     */
    public void type5(View view){
        typeNumber("5");
    }
    /**
     * Calls typeNumber("6")
     * @param view
     */
    public void type6(View view){
        typeNumber("6");
    }
    /**
     * Calls typeNumber("7")
     * @param view
     */
    public void type7(View view){
        typeNumber("7");
    }
    /**
     * Calls typeNumber("8")
     * @param view
     */
    public void type8(View view){
        typeNumber("8");
    }
    /**
     * Calls typeNumber("9")
     * @param view
     */
    public void type9(View view){
        typeNumber("9");
    }
    /**
     * Calls typeNumber("0")
     * @param view
     */
    public void type0(View view){
        typeNumber("0");
    }

    /**
     * Validates the typed value
     * @param view
     */
    public void validate(View view){
        String value = txtValue.getText().toString();
        if(value.length() > 0) {
            int intValue = Integer.parseInt(value);
            if (max < 1) { //Choosing max
                if(intValue > 0){
                    max = intValue;
                    setupGame();
                }
                else {
                    Toast.makeText(this,
                            "La borne doit être supérieure à 0",
                            Toast.LENGTH_SHORT).show();
                }
            }
            else { //Submit value
                triesP1 ++;
                txtTriesP1.setText("Essais : "+triesP1);
                txtValue.setText("");
                if(intValue == mysteryNumber){
                    isGameOver = true;
                    txtMysteryInfo.setText("Le nombre était :");
                    txtClue.setText("Bravo !");
                    txtValue.setText(value);
                    setKeyBoardVisibility(View.INVISIBLE);
                    setEndGameButtonVisibility(View.VISIBLE);

                    if(User.getInstance().isOnline()){
                        API.postScore(this,"mystery_number",triesP1,max);
                    }
                }
                else if(intValue > mysteryNumber){
                    txtClue.setText("C'est moins que "+intValue);
                }
                else {
                    txtClue.setText("C'est plus que " + intValue);
                }
            }
        }
    }

    /**
     * Deletes the last character of the central text's value
     * @param view
     */
    public void backSpace(View view){
        String val = txtValue.getText().toString();
        if(val.length()>0){
            val = val.substring(0,val.length()-1);
            txtValue.setText(val);
        }
    }

    /**
     * Relaunches a game with the same maximum value
     * @param view
     */
    public void replay(View view){
        if(isGameOver){
            setupGame();
            setEndGameButtonVisibility(View.INVISIBLE);
            setKeyBoardVisibility(View.VISIBLE);
        }
    }

    /**
     * Relanches a game from the start (with maximum bound asking)
     * @param view
     */
    public void changeMax(View view){
        if(isGameOver){
            setEndGameButtonVisibility(View.INVISIBLE);
            setKeyBoardVisibility(View.VISIBLE);
            launchSolo(view);
        }
    }

    /**
     * Sets up back the mystery_game main menu
     * @param view
     */
    public void quitGame(View view){
        setContentView(R.layout.activity_mystery_number);
    }

    /**
     * Creates a score activity and launches it (only if User.isOnline())
     * @param view
     */
    public void scores(View view){
        if(User.getInstance().isOnline()) {
            Intent intent = new Intent(this, ScoresActivity.class); //Nav vers la nouvelle activit
            intent.putExtra("gameName", "mystery_number");
            if (max == 0) {
                intent.putExtra("bound", 100);
            } else {
                intent.putExtra("bound", max);
            }
            startActivity(intent);
        }
        else{
            Toast.makeText(this,
                    "Vous devez être connecté !",
                    Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Add the 'number' at the end of the central text
     * @param number the number to add
     */
    private void typeNumber(String number){
        txtValue.setText(txtValue.getText()+number);
    }

    /**
     * Sets up the view for the guessing
     */
    private void setupGame(){
        mysteryNumber = random.nextInt(max+1);
        triesP1 = 0;
        triesP2 = 0;
        txtMaxBound.setText("Borne : "+max);
        txtTriesP1.setText("Essais : 0");
        txtTriesP2.setText("Essais : 0");
        txtValue.setText("");
        txtMysteryInfo.setText("Quel est le nombre mystère ?");
        txtClue.setText("");
    }

    /**
     * Sets up all keyboard buttons visibility to the given mode
     * @param mode visibility (View.VISIBLE, View.INVISIBLE, ...)
     */
    private void setKeyBoardVisibility(int mode){
        btn1.setVisibility(mode);
        btn2.setVisibility(mode);
        btn3.setVisibility(mode);
        btn4.setVisibility(mode);
        btn5.setVisibility(mode);
        btn6.setVisibility(mode);
        btn7.setVisibility(mode);
        btn8.setVisibility(mode);
        btn9.setVisibility(mode);
        btn0.setVisibility(mode);
        btnValidate.setVisibility(mode);
        btnBackSpace.setVisibility(mode);
    }

    /**
     * Sets up all end game buttons visibility to the given mode
     * @param mode visibility (View.VISIBLE, View.INVISIBLE, ...)
     */
    private void setEndGameButtonVisibility(int mode){
        btnReplayMystery.setVisibility(mode);
        btnChangeMax.setVisibility(mode);
        btnQuitMystery.setVisibility(mode);
        btnScoresMystery.setVisibility(mode);
    }

}