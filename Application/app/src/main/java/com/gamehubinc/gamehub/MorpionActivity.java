package com.gamehubinc.gamehub;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class MorpionActivity extends AppCompatActivity {

    public final String p1 = "X";
    public final String p2 = "O";

    private User user;
    private String current;
    private TableLayout grid;
    private TextView txtEnd;
    private String[][] gameGrid;
    private boolean gameIsOver;

    // p2 is computer then
    private boolean comEnabled = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.morpion_activity_main);
    }

    public void leave(View view){
        finish();
    }

    /**
     * Called when pressing on a grid button
     * @param v the pressed button
     */
    public void onButtonClick(View v) {

        if (gameIsOver) {
            return;
        }

        Button btn = (Button)v;
        if (btn.getText().equals("")) {

            // set the button's symbol
            btn.setText(current);
            updateGameGrid();

            next();
        }
    }

    public void startGame(View v) {
        setContentView(R.layout.morpion_game);

        grid = this.findViewById(R.id.morpion_grid);
        txtEnd = this.findViewById(R.id.txt_morpion_end);
        gameGrid = new String[3][3];
        gameIsOver = false;

        if (v.getId() == R.id.btn_m_p1_vs_com) {
            comEnabled = true;
        }

        // p1 starts the game
        current = p1;

    }

    private void next() {

        String winner;
        if ((winner = getWinner()) != null) {
            finishGame(winner);

            return;
        } else if(gridIsFull()) {
            finishGame(null);

            return;
        }

        current = current.equals(p1) ? p2 : p1;

        // if AI enabled and p2, let AI guess next move
        if (comEnabled && current.equals(p2)) {
            playCom();
        }
    }

    public void scores(View view){
        if(User.getInstance().isOnline()){
            Intent intent = new Intent(this, ScoresActivity.class); //Nav vers la nouvelle activit
            intent.putExtra("gameName","morpion");
            startActivity(intent);
        }
        else{
            Toast.makeText(this,
                    "Vous devez être connecté !",
                    Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Let computer decide the next move
     * This is average difficulty (if can win, win, otherwise just take 1st spot)
     */
    private void playCom() {

        int x = -1;
        int y = -1;

        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (gameGrid[i][j] == null) {
                    gameGrid[i][j] = p2;

                    if (x == -1) {
                        x = i;
                        y = j;
                    }

                    String winner = getWinner();
                    if (winner != null && winner.equals(p2)) {

                        // this move makes the com win, do it
                        Button btn = (Button)((TableRow)grid.getChildAt(i)).getChildAt(j);
                        btn.setText(p2);

                        next();

                        return;
                    }

                    gameGrid[i][j] = null;
                }
            }
        }

        // we take the first available spot
        if (gameGrid[1][1] == null) {
            x = 1;
            y = 1;
        }

        gameGrid[x][y] = p2;
        Button btn = (Button)((TableRow)grid.getChildAt(x)).getChildAt(y);
        btn.setText(p2);

        next();
    }

    /**
     * Check if the game is over and returns the winner (or null if draw/not over)
     * @return p1 or p2 or null, depending if there's a winning 3 in a row
     */
    private String getWinner() {

        for (int i = 0; i < 3; i++) {

            // Check rows
            if (gameGrid[i][0] != null
                    && gameGrid[i][0].equals(gameGrid[i][1])
                    && gameGrid[i][0].equals(gameGrid[i][2])) {
                return gameGrid[i][0];
            }

            // Check columns
            if (gameGrid[0][i] != null
                    && gameGrid[0][i].equals(gameGrid[1][i])
                    && gameGrid[0][i].equals(gameGrid[2][i])) {
                return gameGrid[0][i];
            }
        }

        // Check diagonals
        if (gameGrid[0][0] != null
                && gameGrid[0][0].equals(gameGrid[1][1])
                && gameGrid[0][0].equals(gameGrid[2][2])) {
            return gameGrid[0][0];
        }
        if (gameGrid[0][2] != null
                && gameGrid[0][2].equals(gameGrid[1][1])
                && gameGrid[0][2].equals(gameGrid[2][0])) {
            return gameGrid[0][2];
        }

        return null;
    }

    /**
     * Fill gameGrid array using the TableLayout values
     * for easier use
     */
    private void updateGameGrid() {
        for (int i = 0; i < grid.getChildCount(); i++) {
            TableRow row = (TableRow)grid.getChildAt(i);

            for (int j = 0; j < row.getChildCount(); j++) {
                Button btn = (Button)row.getChildAt(j);

                if (!btn.getText().equals("")) {
                    gameGrid[i][j] = btn.getText().toString();
                } else {
                    gameGrid[i][j] = null;
                }
            }
        }
    }

    /**
     * Check if all buttons are filled
     * @return true if yes, false if there's buttons left
     */
    private boolean gridIsFull() {

        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (gameGrid[i][j] == null) {
                    return false;
                }
            }
        }

        return true;
    }

    private void finishGame(String winner) {
        gameIsOver = true;

        // disable buttons
        for (int i = 0; i < grid.getChildCount(); i++) {
            TableRow row = (TableRow)grid.getChildAt(i);

            for (int j = 0; j < row.getChildCount(); j++) {
                Button btn = (Button)row.getChildAt(j);
                btn.setEnabled(false);
            }
        }

        // fill text
        if (winner == null) {
            txtEnd.setText(R.string.txt_morpion_draw);
        } else {
            String txt = getString(R.string.txt_morpion_wins, winner);
            txtEnd.setText(txt);
            if(User.getInstance().isOnline() && comEnabled && winner == "X"){
                API.putMorpionScore(this);
            }
        }

        // make continue/leave buttons visible
        findViewById(R.id.btn_m_continue).setVisibility(View.VISIBLE);
        findViewById(R.id.btn_m_leave).setVisibility(View.VISIBLE);
    }

    public void quit(View v) {
        setContentView(R.layout.morpion_activity_main);
        comEnabled = false;
    }

    public void restart(View v) {

        // enable buttons
        for (int i = 0; i < grid.getChildCount(); i++) {
            TableRow row = (TableRow)grid.getChildAt(i);

            for (int j = 0; j < row.getChildCount(); j++) {
                Button btn = (Button)row.getChildAt(j);
                btn.setEnabled(true);
                btn.setText("");
            }
        }

        // make continue/leave buttons invisible
        findViewById(R.id.btn_m_continue).setVisibility(View.INVISIBLE);
        findViewById(R.id.btn_m_leave).setVisibility(View.INVISIBLE);

        gameIsOver = false;
        gameGrid = new String[3][3];
        txtEnd.setText("");
        current = p1;
    }
}
