package com.gamehubinc.gamehub;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class HomePageActivity extends AppCompatActivity {

    private User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_page);
        TextView mTextView = findViewById(R.id.txt_homePagePseudo);
        mTextView.setText(User.getInstance().getName());
    }

    public void onClickBtn(View view){
        Intent intent;
        switch(view.getId()){
            case R.id.btn_chifoumi:
                intent = new Intent(this, ChifoumiActivity.class); //Nav vers la nouvelle activit
                startActivity(intent);
                break;
            case R.id.btn_morpion:
                intent = new Intent(this, MorpionActivity.class);
                startActivity(intent);
                break;
            case R.id.btn_mystery:
                intent = new Intent(this, MysteryNumberActivity.class); //Nav vers la nouvelle activit
                startActivity(intent);
                break;

            case R.id.btn_clicker:
                intent = new Intent(this, ClickerActivity.class); //Nav vers la nouvelle activit
                startActivity(intent);
                break;
        }
    }

    public void changeUser(View view){
        User.reset();
        finish();
    }

}