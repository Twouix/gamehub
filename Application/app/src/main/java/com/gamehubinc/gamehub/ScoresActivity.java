package com.gamehubinc.gamehub;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ScoresActivity extends AppCompatActivity {

    private TextView txtScoresTitle, txtScores, txtScoresBound;
    private String gameName, title, getQuery = "";
    private EditText editTextNumberBound;
    private Button btnValidateBound;
    private boolean isPersonnal = false;
    private static final int MAX_DISPLAYED = 20;
    private int bound = 100;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scores);
        txtScoresTitle = this.findViewById(R.id.txtScoresTitle);
        txtScores = this.findViewById(R.id.txtScores);
        txtScoresBound = this.findViewById(R.id.txtScoresBound);
        editTextNumberBound = this.findViewById(R.id.editTextNumberBound);
        btnValidateBound = this.findViewById(R.id.btnValidateBound);
        Intent intent = getIntent();
        if (intent != null){
            gameName = intent.getStringExtra("gameName");
            switch(gameName){
                case "chifoumi" :
                    title = "Chifoumi";
                break;

                case "mystery_number" :
                    title = "Nombre mystère";
                    bound = intent.getIntExtra("bound",100);
                    getQuery = "?bound="+bound;
                    editTextNumberBound.setText(""+bound);
                    txtScoresBound.setVisibility(View.VISIBLE);
                    btnValidateBound.setVisibility(View.VISIBLE);
                    editTextNumberBound.setVisibility(View.VISIBLE);
                break;

                case "morpion" :
                    title = "Morpion";
                break;

                case "clicker_hero" :
                    title = "Clicker boy";
                break;

                default :
                    title = "Scores";
            }
            txtScoresTitle.setText(title);
        }
        getWorld(null);
    }

    /**
     * Gets the world scores for the 'gameName' game
     * @param view
     */
    public void getWorld(View view){
        isPersonnal = false;
        API.getScores(this,gameName,getQuery);
    }

    /**
     * Gets the user's scores for the 'gameName' game
     * @param view
     */
    public void getPersonnal(View view){
        isPersonnal = true;
        API.getScores(this,gameName,true,getQuery);
    }

    /**
     * Refreshes the 'bound' value and recalls the get scores (for mystery_number)
     * @param view
     */
    public void refreshBound(View view){
        if(gameName.equals("mystery_number")){
            bound = Integer.parseInt(editTextNumberBound.getText().toString());
            getQuery = "?bound="+bound;
            if(isPersonnal){
                getPersonnal(view);
            }
            else{
                getWorld(view);
            }
        }
    }

    /**
     * Displays the scores
     * @param scores JSONObject : {data:[{username:"...",score:X},{...},{...}]}
     */
    public void displayScores(JSONObject scores){
        String scoresTxt = processScores(scores);
        txtScores.setText(scoresTxt);
    }

    public void leave(View view){
        finish();
    }

    /**
     * Transform the JSONObject scores to a formatted string
     * @param scores JSONObject : {data:[{username:"...",score:X},{...},{...}]}
     * @return Scores as a formatted string
     */
    private String processScores(JSONObject scores){
        String res = "";
        try {
            JSONArray data = scores.getJSONArray("data");
            for(int i = 0; i< data.length() && i<MAX_DISPLAYED ; i++){
                JSONObject obj = (JSONObject) data.get(i);
                res+= (i+1) +"\t\t"+ obj.getString("username") + "\t\t" + obj.getInt("score") + "\n";
            }
        }
        catch(JSONException e){
            Toast.makeText(this,
                    "Erreur lors de l'affichage des scores",
                    Toast.LENGTH_SHORT).show();
        }

        return res;
    }


}