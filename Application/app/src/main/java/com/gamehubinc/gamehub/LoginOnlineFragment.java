package com.gamehubinc.gamehub;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONObject;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link LoginOnlineFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class LoginOnlineFragment extends Fragment {

    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String mParam1;
    private String mParam2;

    public LoginOnlineFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment LoginOnlineFragment.
     */
    public static LoginOnlineFragment newInstance(String param1, String param2) {
        LoginOnlineFragment fragment = new LoginOnlineFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_login_online, container, false);

        // Inflate the layout for this fragment
        return v;
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        view.findViewById(R.id.btn_back_loginOnline).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavHostFragment.findNavController(LoginOnlineFragment.this)
                        .navigate(R.id.action_loginOnlineFragment_to_FirstFragment);
            }
        });

        view.findViewById(R.id.btn_create_acc).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavHostFragment.findNavController(LoginOnlineFragment.this)
                        .navigate(R.id.action_loginOnlineFragment_to_createAccFragment);
            }
        });

        View.OnClickListener listenerLogin=new View.OnClickListener() {
            @Override
            public void onClick(View vi) {

                //Getting username
                EditText field_pseudo = (EditText)view.findViewById(R.id.input_pseudo_online);
                String pseudo = field_pseudo.getText().toString();

                //Getting password
                EditText field_mdp = (EditText)view.findViewById(R.id.input_pass_online);
                String mdp = field_mdp.getText().toString();

                if (!pseudo.matches("") || !mdp.matches("")) {//Login
                    API.logIn(getContext(), pseudo, mdp);
                }else {
                    Toast.makeText(getContext(),
                            "Vous devez renseigner un Pseudo et votre Mot de passe ... \uD83E\uDD37\u200D♂️ ",
                            Toast.LENGTH_SHORT).show();
                }

            }
        };

        Button btn =(Button) view.findViewById(R.id.btn_login_online);
        btn.setOnClickListener(listenerLogin);

    }
}