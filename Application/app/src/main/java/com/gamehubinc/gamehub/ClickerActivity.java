package com.gamehubinc.gamehub;

import androidx.appcompat.app.AppCompatActivity;
import androidx.navigation.fragment.NavHostFragment;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

public class ClickerActivity extends AppCompatActivity {

    private User user;
    private int score,timeMode;
    boolean isGameOver, isMatchOver;
    private Button btnHit,btnEndReplay,btnEndLeave;
    private TextView timeRemainingdisp, scoreDisp, finalscore, finalMode;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_clicker_main);
    }

    public void leave(View view){
        finish();
    }

    public void LoadMenu(View view){
        setContentView(R.layout.activity_clicker_main);
    }

    public void launch10sec(View view){
        loadGame(view,10);
    }

    public void scores(View view){
        if(User.getInstance().isOnline()) {
            Intent intent = new Intent(this, ScoresActivity.class); //Nav vers la nouvelle activit
            intent.putExtra("gameName", "clicker_hero");
            startActivity(intent);
        }
        else{
            Toast.makeText(this,
                    "Vous devez être connecté !",
                    Toast.LENGTH_SHORT).show();
        }
    }

    private void loadGame(View view, int mode){
        setContentView(R.layout.clicker_game);
        isGameOver=false;
        score = 0;
        timeMode = mode;
        isMatchOver = false;
        timeRemainingdisp = this.findViewById(R.id.txtTimeClicker);
        scoreDisp = this.findViewById(R.id.TimeClickerScore);
        btnHit = this.findViewById(R.id.btnHit);
        btnHit.setText("GO");
        btnHit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startWarmUp(view);
            }
        });
    }

    private void startWarmUp(View view){
          new CountDownTimer(6000, 1000) {
            public void onTick(long millisUntilFinished) {
                btnHit.setText(""+millisUntilFinished / 1000);
            }
            public void onFinish() {
                clickerPlay(view);
            }
          }.start();

          btnHit.setOnClickListener(null);
    }

    private void clickerPlay(View view){

        btnHit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                score ++;
                scoreDisp.setText(""+score);
            }
        });

        btnHit.setText("TAPE MOI ÇA!");

        new CountDownTimer(11000, 1000) {
            public void onTick(long millisUntilFinished) {
                timeRemainingdisp.setText(""+millisUntilFinished / 1000);
            }
            public void onFinish() {
                btnHit.setText("STOP !!");
                btnHit.setOnClickListener(null);
                clickerBeforeEndGame(view);
            }
        }.start();

    }
    // petite tempo de 3 sec
    private void clickerBeforeEndGame(View view){
        new CountDownTimer(3000, 1000) {
            public void onTick(long millisUntilFinished) {
            }
            public void onFinish() {
                clickerEndGame(view);
            }
        }.start();
    }

    private void clickerEndGame(View view){
        setContentView(R.layout.clicker_endgame);

        finalscore = this.findViewById(R.id.txtEndScoreClicker);
        finalscore.setText("Score : "+score);

        finalMode = this.findViewById(R.id.txtEndModeClicker);
        finalMode.setText("Mode : "+timeMode+ " sec");

        btnEndLeave = this.findViewById(R.id.btnLeaveEndClicker);
        btnEndReplay = this.findViewById(R.id.btnReplayClicker);

        if(User.getInstance().isOnline()){
            API.postScore(this,"clicker_hero",score);
        }

        btnEndLeave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LoadMenu(view);
            }
        });

        btnEndReplay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loadGame(view,timeMode);
            }
        });

    }


}