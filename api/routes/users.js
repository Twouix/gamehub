var express = require("express");
var router = express.Router();
var db = require("../database/connection");
var hasher = require("password-hash");

/**
 * Get all users
 * /users
 */
router.get("/", function(req, res) {
    db.query("SELECT id, username FROM users", function (err, rows) {

        if (err) {
            return res.status(500).json(err);
        }

        return res.json(rows);
    });
});

/**
 * Get an user by id
 * /users/{id}
 */
router.get("/:id", function (req, res) {
    const userId = req.params.id;

    if (!userId) {
        return res.sendStatus(400);
    }
    
    db.query("SELECT id, username FROM users WHERE id = ?", [userId], function (err, rows) {

        if (err) {
            return res.status(500).json(err);
        }

        if (rows.length) {
            return res.json(rows[0]);
        } else {
            return res.sendStatus(404);
        }
    });
});

/**
 * Create an user
 * /users
 * JSON body required:
 * {
 *   username: string, // attention: unique
 *   password: string
 * }
 */
router.post("/", function (req, res) {
    const {username, password} = req.body;

    if (!username || !password) {
        return res.sendStatus(400);
    }

    let hashPassword = hasher.generate(password);

    db.query("INSERT INTO users VALUES(NULL, ?, ?)", [username, hashPassword], function (err, rows) {

        if (err) {
            if (err.sqlState === "23000") {
                return res.status(400).json({message: "Un utilisateur avec ce pseudo existe déjà."});
            } else {
                return res.status(500).json(err);
            }
        }

        return res.sendStatus(200);
    });
});

/**
 * Delete an user by id
 * /users/{id}
 */
router.delete("/:id", function (req, res) {
    const userId = req.params.id;

    if (!userId) {
        return res.sendStatus(400);
    }

    db.query("DELETE FROM users WHERE id = ?", [userId], function (err, rows) {
        if (err) {
            return res.status(500).json(err);
        }

        return res.sendStatus(200);
    });
});

/**
 * Get an user by username/password
 * /users/verify
 * JSON body required:
 * {
 *   username: string,
 *   password: string
 * }
 */
router.post("/verify", function (req, res) {

    const {username, password} = req.body;
    
    if (!username || !password) {
        return res.sendStatus(400);
    }
    
    db.query("SELECT * FROM users WHERE username = ?", [username], function (err, rows) {

        if (err) {
            return res.status(500).json(err);
        }

        if (rows.length) {
            if (hasher.verify(password, rows[0].password)) {
                return res.json({
                    "id": rows[0].id,
                    "username": rows[0].username,
                });
            } else {
                return res.sendStatus(404);
            }
        } else {
            return res.sendStatus(404);
        }
    });
});

module.exports = router;
