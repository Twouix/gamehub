var express = require("express");
var router = express.Router();
var db = require("../database/connection");

/**
 * Add a victory for an user in Morpion
 * /scores/morpion/{id}
 */
router.put("/morpion/:id", function (req, res) {
    const userId = req.params.id;
    
    if (!userId) {
        return res.sendStatus(400);
    }

    const sql = "INSERT INTO morpion VALUES (NULL, ?, 1) ON DUPLICATE KEY UPDATE score = score + 1";
    db.query(sql, [userId], function (err) {
        if (err) {
            return res.status(500).json(err);
        }

        return res.sendStatus(200);
    });
});

/**
 * Get all scores for Morpion
 * /scores/morpion
 */
router.get("/morpion", function (req, res) {
    db.query("SELECT m.*, u.username AS 'username' FROM morpion m JOIN users u ON u.id = m.user_id ORDER BY m.score DESC", function (err, rows) {
        if (err) {
            return res.status(500).json(err);
        }

        return res.json({'data': rows});
    });
});

/**
 * Get Morpion score for 1 user
 * /scores/morpion/{id}
 */
router.get("/morpion/:id", function (req, res) {
    const userId = req.params.id;

    if (!userId) {
        return res.sendStatus(400);
    }

    db.query("SELECT m.*, u.username AS 'username' FROM morpion m JOIN users u ON u.id = m.user_id WHERE m.user_id = ? ORDER BY m.score DESC", [userId], function (err, rows) {
        if (err) {
            return res.status(500).json(err);
        }

        return res.json({'data': rows});
    });
});

/**
 * Add a score for an user in Chifoumi
 * /scores/chifoumi
 */
router.post("/chifoumi", function (req, res) {
    const {userId, score} = req.body;

    if (!userId || score === undefined) {
        return res.sendStatus(400);
    }
    
    const sql = "INSERT INTO chifoumi VALUES (NULL, ?, ?)";
    db.query(sql, [userId, score], function (err, rows) {
        if (err) {
            return res.status(500).json(err);
        }

        return res.sendStatus(200);
    });
});

/**
 * Get all scores for Chifoumi
 * /scores/chifoumi
 */
router.get("/chifoumi", function (req, res) {
    db.query("SELECT MAX(c.score) AS score, c.user_id, u.username AS 'username' FROM chifoumi c JOIN users u ON u.id = c.user_id GROUP BY c.user_id ORDER BY score DESC", function (err, rows) {
        if (err) {
            return res.status(500).json(err);
        }

        return res.json({'data': rows});
    });
});

/**
 * Get Chifoumi scores for 1 user
 * /scores/chifoumi/{id}
 */
router.get("/chifoumi/:id", function (req, res) {
    const userId = req.params.id;

    if (!userId) {
        return res.sendStatus(400);
    }

    db.query("SELECT c.*, u.username AS 'username' FROM chifoumi c JOIN users u ON u.id = c.user_id WHERE c.user_id = ? ORDER BY c.score DESC", [userId], function (err, rows) {
        if (err) {
            return res.status(500).json(err);
        }

        return res.json({'data': rows});
    });
});

/**
 * Add a score for an user in Mystery Number
 * /scores/mystery_number
 */
router.post("/mystery_number", function (req, res) {
    const {userId, score, bound} = req.body;

    if (!userId ||  score === undefined || bound === undefined) {
        return res.sendStatus(400);
    }
    
    const sql = "INSERT INTO nombre_mystere VALUES (NULL, ?, ?, ?)";
    db.query(sql, [userId, score, bound], function (err, rows) {
        if (err) {
            return res.status(500).json(err);
        }

        return res.sendStatus(200);
    });
});

/**
 * Get all scores for Mystery Number
 * /scores/mystery_number
 */
router.get("/mystery_number", function (req, res) {
    const bound = req.query.bound;
    
    if (bound === undefined) {
        return res.sendStatus(400);
    }

    const sql = `
        SELECT MIN(n.score) AS score, n.user_id, u.username AS 'username'
        FROM nombre_mystere n
        JOIN users u ON u.id = n.user_id
        WHERE n.bound = ?
        GROUP BY n.user_id
        ORDER BY score ASC
    `;

    db.query(sql, [bound], function (err, rows) {
        if (err) {
            return res.status(500).json(err);
        }

        return res.json({'data': rows});
    });
});

/**
 * Get Mystery Number scores for 1 user
 * /scores/mystery_number/{id}
 */
router.get("/mystery_number/:id", function (req, res) {
    const userId = req.params.id;
    const bound = req.query.bound;

    if (!userId || bound === undefined) {
        return res.sendStatus(400);
    }

    db.query("SELECT n.*, u.username AS 'username' FROM nombre_mystere n JOIN users u ON u.id = n.user_id WHERE n.user_id = ? AND n.bound = ? ORDER BY n.score ASC", [userId, bound], function (err, rows) {
        if (err) {
            return res.status(500).json(err);
        }

        return res.json({'data': rows});
    });
});

/**
 * Add a score for an user in Clicker Hero
 * /scores/clicker_hero
 */
router.post("/clicker_hero", function (req, res) {
    const {userId, score} = req.body;
    
    if (!userId || score === undefined) {
        return res.sendStatus(400);
    }
    
    const sql = "INSERT INTO clicker_hero VALUES (NULL, ?, ?)";
    db.query(sql, [userId, score], function (err, rows) {
        if (err) {
            return res.status(500).json(err);
        }

        return res.sendStatus(200);
    });
});

/**
 * Get all scores for Clicker Hero
 * /scores/clicker_hero
 */
router.get("/clicker_hero", function (req, res) {
    db.query("SELECT MAX(c.score) AS score, c.user_id, u.username AS 'username' FROM clicker_hero c JOIN users u ON u.id = c.user_id GROUP BY c.user_id ORDER BY score DESC", function (err, rows) {
        if (err) {
            return res.status(500).json(err);
        }

        return res.json({'data': rows});
    });
});

/**
 * Get Clicker Hero scores for 1 user
 * /scores/clicker_hero/{id}
 */
router.get("/clicker_hero/:id", function (req, res) {
    const userId = req.params.id;

    if (!userId) {
        return res.sendStatus(400);
    }

    db.query("SELECT c.*, u.username AS 'username' FROM clicker_hero c JOIN users u ON u.id = c.user_id WHERE c.user_id = ? ORDER BY c.score DESC", [userId], function (err, rows) {
        if (err) {
            return res.status(500).json(err);
        }

        return res.json({'data': rows});
    });
});

module.exports = router;
