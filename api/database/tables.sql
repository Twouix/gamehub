CREATE TABLE `users` (
    `id` INT NOT NULL AUTO_INCREMENT,
    `username` VARCHAR(255) NOT NULL UNIQUE,
    `password` VARCHAR(255) NOT NULL,

    PRIMARY KEY(`id`)
);

CREATE TABLE `morpion` (
    `id` INT NOT NULL AUTO_INCREMENT,
    `user_id` INT NOT NULL UNIQUE,
    `score` INT NOT NULL DEFAULT 0, -- nombre de victoires

    PRIMARY KEY (`id`),
    CONSTRAINT fk_user_morpion FOREIGN KEY (`user_id`) REFERENCES users(`id`) ON DELETE CASCADE
);

CREATE TABLE `chifoumi` (
    `id` INT NOT NULL AUTO_INCREMENT,
    `user_id` INT NOT NULL,
    `score` INT NOT NULL DEFAULT 0, -- score mort subite

    PRIMARY KEY (`id`),
    CONSTRAINT fk_user_chifoumi FOREIGN KEY (`user_id`) REFERENCES users(`id`) ON DELETE CASCADE
);

CREATE TABLE `nombre_mystere` (
    `id` INT NOT NULL AUTO_INCREMENT,
    `user_id` INT NOT NULL,
    `score` INT NOT NULL, -- nombre de tentatives
    `bound` INT NOT NULL,

    PRIMARY KEY (`id`),
    CONSTRAINT fk_user_nombre_mystere FOREIGN KEY (`user_id`) REFERENCES users(`id`) ON DELETE CASCADE
);

CREATE TABLE `clicker_hero` (
    `id` INT NOT NULL AUTO_INCREMENT,
    `user_id` INT NOT NULL,
    `score` INT NOT NULL, -- nombre de clics/10 secondes

    PRIMARY KEY (`id`),
    CONSTRAINT fk_user_clicker_hero FOREIGN KEY (`user_id`) REFERENCES users(`id`) ON DELETE CASCADE
);
